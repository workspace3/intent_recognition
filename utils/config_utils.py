# _*_ coding: utf-8 _*-

import os
import logging
import traceback
from configparser import ConfigParser


logger = logging.getLogger(__name__)


def get_config(config_file='conf/const_config.ini'):
    try:
        project_path = os.path.normpath(os.path.join(
            os.path.dirname(os.path.abspath(__file__)), '../../'))
        parser = ConfigParser()
        parser.read(os.path.join(project_path, config_file), encoding="utf-8")
        # get the ints, floats and strings, lists
        _conf_ints = [(key, int(value)) for key, value in parser.items('ints')]
        _conf_floats = [(key, float(value))
                        for key, value in parser.items('floats')]
        _conf_files = [(key, str(value))
                       for key, value in parser.items('files')]
        _conf_strings = [(key, str(value))
                         for key, value in parser.items('strings')]
        _conf_lists = [(key, eval(value))
                       for key, value in parser.items('lists')]
        return dict(_conf_ints + _conf_floats + _conf_files + _conf_strings + _conf_lists)
    except Exception:
        logger.error(traceback.format_exc())


class Config(object):
    def __init__(self, project_path, conf_path):
        self._project_path = project_path
        self._conf_path = os.path.join(self._project_path, conf_path)
        self.parser = ConfigParser()
        self.parser.read(self._conf_path)

    def get_value(self, section, key):
        try:
            res = str(self.parser.get(section, key))
            return res
        except Exception:
            logger.error(traceback.format_exc())

    def get_items(self, section):
        try:
            res = self.parser.items(section)
            return dict((x, y) for x, y in res)
        except Exception:
            logger.error(traceback.format_exc())

    def get_sections(self):
        try:
            res = self.parser.sections()
            return res
        except Exception:
            logger.error(traceback.format_exc())

    def get_options(self, section):
        try:
            res = self.parser.options(section)
            return res
        except Exception:
            logger.error(traceback.format_exc())


if __name__ == '__main__':
    pass
