# -*- coding: utf-8 -*-
"""
意图识别模块
借鉴bojone大神的bert4keras(version=0.7.8)

@date: 2020/7/16
@author: wen
"""
import os
import sys
import logging
import tensorflow as tf
from tensorflow.python.keras.backend import set_session
from bert4keras.backend import keras, set_gelu
from bert4keras.tokenizers import Tokenizer
from bert4keras.models import build_transformer_model
from bert4keras.optimizers import Adam, extend_with_piecewise_linear_lr
from bert4keras.snippets import sequence_padding, DataGenerator
from bert4keras.snippets import open as open_in_bert4keras
from keras.layers import Lambda, Dense

curr_dir = os.path.dirname(__file__)
project_dir = os.path.normpath(
    os.path.join(curr_dir, "..")
)
sys.path.insert(0, project_dir)

from utils.logger_utils import setup_logging

pre_train_model_path = os.path.normpath(
    os.path.join(project_dir, "data/albert_large")
)

DEFAULT_KWARGS = {
        "labels": ["0", "1"],
        "model_type": "albert",
        "config_path": os.path.join(pre_train_model_path, "albert_config.json"),
        "checkpoint_path": os.path.join(pre_train_model_path, "model.ckpt-best"),
        "model_path": os.path.join(project_dir,
                                   "models/model_v1.1.0_20200714.weights"),
        "vocab": os.path.join(pre_train_model_path, "vocab_chinese.txt"),
}

set_gelu('tanh')  # 切换gelu版本
epochs = 20
max_len = 128
batch_size = 32
lr = 1e-5

#
setup_logging()


class ClfDataGenerator(DataGenerator):
    """数据生成器
    """
    def __init__(self, data, batch_size, tokenizer):
        super(ClfDataGenerator, self).__init__(data=data, batch_size=batch_size)
        self.tokenizer = tokenizer

    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids, batch_labels = [], [], []
        for is_end, (label, text_a, text_b) in self.sample(random):
            token_ids, segment_ids = self.tokenizer.encode(text_a, text_b,
                                                           max_length=max_len)
            batch_token_ids.append(token_ids)
            batch_segment_ids.append(segment_ids)
            batch_labels.append([label])
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                batch_labels = sequence_padding(batch_labels)
                yield [batch_token_ids, batch_segment_ids], batch_labels
                batch_token_ids, batch_segment_ids, batch_labels = [], [], []


def evaluate(data, model):
    """
    测试模型效果
    :param data:
    :param model:
    :return:
    """
    total, right = 0., 0.
    for x_true, y_true in data:
        y_pred = model.predict(x_true).argmax(axis=-1)
        y_true = y_true[:, 0]
        total += len(y_true)
        right += (y_true == y_pred).sum()
    return right / total


class Evaluator(keras.callbacks.Callback):
    def __init__(self, the_model, model_path,
                 dev_generator, test_generator):
        super(Evaluator, self).__init__()
        self.best_val = -float('inf')
        self.model = the_model
        self.model_path = model_path
        self.dev_generator = dev_generator
        self.test_generator = test_generator

    def on_epoch_end(self, epoch, logs=None):
        val_acc = evaluate(self.dev_generator, self.model)
        loss = logs["loss"]
        val = val_acc * 1 - loss * 0
        if val > self.best_val:
            self.best_val = val
            self.model.save_weights(self.model_path)
        test_acc = evaluate(self.test_generator, self.model)
        logging.info(
            u'%s, val_acc: %.5f, best_val: %.5f, test_acc: %.5f\n' %
            (self.model_path, val_acc, self.best_val, test_acc)
        )


class IntentRecognition:
    def __init__(self, kwargs=None):
        """
        :param kwargs:
        {
                ---------不可缺省参数----------
                "labels": list, 标签集
                "model_type": str, "bert" or "albert"
                "config_path": str, 预训练模型的配置文件路径
                "vocab": str, 预训练模型的词汇表路径
                ----------在训练/预测时均不可缺省---------
                "model_path": str, 训练好的判别模型路径
                ----------在训练时不可缺省，预测时可缺省---------
                "checkpoint_path": str, 预训练模型的checkpoint路径
                "train_path": str, 训练集文件路径
                "dev_path": str, 验证集文件路径
                "test_path": str, 测试集文件路径
        }
        """
        # 如果没初始化入参，那么就采用默认入参
        if kwargs is None:
            kwargs = DEFAULT_KWARGS
        #
        labels = kwargs.get("labels", [])
        self.label2id = dict([(n, i) for i, n in enumerate(labels)])
        self.id2label = dict(enumerate(labels))
        #
        self.model_type = kwargs.get("model_type", "bert")
        self.config_path = kwargs.get("config_path", "")
        self.checkpoint_path = kwargs.get("checkpoint_path", "")
        self.model_path = kwargs.get("model_path", "")
        vocab_path = kwargs.get("vocab", "")
        # 建立分词器
        self.tokenizer = Tokenizer(vocab_path, do_lower_case=True)
        #
        self.train_path = kwargs.get("train_path", "")
        self.dev_path = kwargs.get("dev_path", "")
        self.test_path = kwargs.get("test_path", "")
        #
        self.session = tf.Session()
        self.graph = tf.get_default_graph()
        set_session(self.session)
        self._create_model()
        if os.path.exists(self.model_path):
            self.load_model(self.model_path)
            logging.info("加载模型成功：{}".format(self.model_path))
        else:
            logging.error("加载模型失败：{}".format(self.model_path))

    def load_model(self, model_path):
        """
        加载训练好的判别模型
        :param model_path: 模型路径
        :return:
        """
        self.model.load_weights(model_path)

    def train_and_test(self):
        """
        线下训练和测试模型
        :return:
        """
        train_data = self._load_data(self.train_path)
        train_len = len(train_data)
        train = ClfDataGenerator(train_data, tokenizer=self.tokenizer,
                                 batch_size=batch_size)
        dev = ClfDataGenerator(self._load_data(self.dev_path), tokenizer=self.tokenizer,
                               batch_size=batch_size)
        test = ClfDataGenerator(self._load_data(self.test_path), tokenizer=self.tokenizer,
                                batch_size=batch_size)
        del train_data
        evaluator = Evaluator(the_model=self.model,
                              model_path=self.model_path,
                              dev_generator=dev,
                              test_generator=test)
        self.model.fit_generator(
            train.forfit(),
            steps_per_epoch=train_len // batch_size,
            epochs=epochs,
            callbacks=[evaluator]
        )

    def predict(self, text_a, text_b=None):
        """
        线上预测
        :param text_a: str, 对话历史
        :param text_b: str, 当前用户输入
        :return:
        {
            "confidence": 预测标签的置信度,
            "label": 预测标签
        }
        """
        token_ids, segment_ids = self.tokenizer.encode(
            first_text=text_a, second_text=text_b)
        with self.graph.as_default():
            set_session(self.session)
            result = self.model.predict([[token_ids], [segment_ids]])
            probability = result.max(axis=-1)[0]
            label_id = result.argmax(axis=-1)[0]
            label = self.id2label.get(label_id)
        return {"confidence": probability, "label": label}

    def _create_model(self):
        """
        创建模型结构
        :return:
        """
        # 加载预训练模型
        bert = build_transformer_model(
            config_path=self.config_path,
            checkpoint_path=self.checkpoint_path,
            model=self.model_type,
            return_keras_model=False,
        )

        output = Lambda(lambda x: x[:, 0], name='CLS-token')(bert.model.output)
        output = Dense(
            units=len(self.label2id),
            activation='softmax',
            kernel_initializer=bert.initializer
        )(output)

        model = keras.models.Model(bert.model.input, output)
        model.summary()

        # 派生为带分段线性学习率的优化器。
        # 其中name参数可选，但最好填入，以区分不同的派生优化器。
        AdamLR = extend_with_piecewise_linear_lr(Adam, name='AdamLR')

        model.compile(
            loss='sparse_categorical_crossentropy',
            # optimizer=Adam(1e-5),  # 用足够小的学习率
            optimizer=AdamLR(learning_rate=lr, lr_schedule={
                1000: 1,
                2000: 0.1
            }),
            metrics=['accuracy'],
        )
        self.model = model

    def _load_data(self, filename):
        """
        加载数据集
        --------
        数据格式:
        每行为一条数据，用Tab隔开， 依次为 label, text_a, text_b
        其中text_b可缺省。
        --------
        :param filename:
        :return:
        """
        D = []
        with open_in_bert4keras(filename, encoding='utf-8') as f:
            for l in f:
                parts = l.strip().split('\t')
                if len(parts) == 2:
                    label, text_a = parts
                    text_b = None
                elif len(parts) == 3:
                    label, text_a, text_b = parts
                else:
                    continue
                D.append((self.label2id.get(label), text_a, text_b))
        return D


if __name__ == "__main__":
    import timeit
    # is_training = False
    # albert_type = "small"
    # task_name = "beauty_question_only_first"
    # with open("data/{}/labels.txt".format(task_name), encoding="utf8") as fi:
    #     labels = [line.strip() for line in fi.readlines() if len(line.strip()) > 0]
    # config_path = 'models/albert_{}/albert_config.json'.format(albert_type)
    # checkpoint_path = 'models/albert_{}/model.ckpt-best'.format(albert_type)
    # dict_path = 'models/albert_{}/vocab_chinese.txt'.format(albert_type)
    # model_path = 'models/keras/{}_{}.weights'.format(albert_type, task_name)
    # # 加载数据集
    # train_file = 'data/{}/train.tsv'.format(task_name)
    # valid_file = 'data/{}/dev.tsv'.format(task_name)
    # test_file = 'data/{}/test.tsv'.format(task_name)
    #
    is_training = True
    pre_train_model_path = os.path.normpath(
        os.path.join(project_dir, "data/albert_large")
    )
    DEFAULT_KWARGS.update({
        "labels": ['护肤步骤', '询问产品', '询问产品的成分', '询问功效', '询问包含某成分的产品', '询问原因', '询问口碑', '询问安全等级', '询问状态（拍照测肤）', '询问致痘风险', '询问解决方案（知识）'],
        "model_type": "albert",
        "config_path": os.path.join(pre_train_model_path, "albert_config.json"),
        "checkpoint_path": os.path.join(pre_train_model_path, "model.ckpt-best"),
        "vocab": os.path.join(pre_train_model_path, "vocab_chinese.txt"),
        "model_path": os.path.join(project_dir,
                                   "models/clf_albert_large_model_v1.0.0_20200720.weights"),
        "train_path": os.path.join(project_dir, "data/intent_recog_data/train.tsv"),
        "dev_path": os.path.join(project_dir, "data/intent_recog_data/dev.tsv"),
        "test_path": os.path.join(project_dir, "data/intent_recog_data/test.tsv"),
    })
    #
    my_model = IntentRecognition(DEFAULT_KWARGS)
    if is_training:
        my_model.train_and_test()
    flag = "y"
    while "y" == flag:
        t_a = input("please input text_a: ")
        # t_b = input("please input text_b: ")
        s = timeit.default_timer()
        result = my_model.predict(t_a)
        e = timeit.default_timer()
        print("predicted label: {}, probability: {:.5f}, time cost: {:.5f}".format(
            result["label"], result["confidence"], e - s))
        flag = input("go on? y/N")
    print("done.")
